<?php

Route::get('pretest/{value}',[

      'uses' => 'PretestController@convertNumberAbbreviator',
      ]);

Route::get('upload','PretestController@showForm');
Route::post('upload','PretestController@store');

Route::get('/', function () {
    return view('welcome');
});
