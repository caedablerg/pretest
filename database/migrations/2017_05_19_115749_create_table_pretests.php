<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePretests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pretests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('BBRANCHID');
            $table->string('CGID');
            $table->string('BRANCHFULLNAME');
            $table->string('BRANCHINITIALNAME');
            $table->integer('COMPANYID');
            $table->string('BRANCHADDRESS');
            $table->string('BRANCHRT');
            $table->string('BRANCHRW');
            $table->string('BRANCHKELURAHAN');
            $table->string('BRANCHKECAMATAN');
            $table->string('BRANCHCITY');
            $table->string('BRANCHZIPCODE');
            $table->string('BRANCHAREAPHONE1');
            $table->string('BRANCHPHONE1');
            $table->string('BRANCHAREAPHONE2');
            $table->string('BRANCHPHONE2');
            $table->string('BRANCHAREAFAX');
            $table->string('BRANCHFAX');
            $table->string('CONTACTPERSONNAME');
            $table->string('CONTACTPERSONJOBTITLE');
            $table->string('CONTACTPERSONEMAIL');
            $table->string('CONTACTPERSONHP');
            $table->string('BRANCHMANAGERNAME');
            $table->string('ADHNAME');
            $table->string('ARCONTROLNAME');
            $table->string('ASSETDOCCUSTODIANNAME');
            $table->string('BRANCHSTATUS');
            $table->string('ISCASHIERCLOSED');
            $table->string('ISHEADOFFICE');
            $table->string('ACCOUNTNAME');
            $table->string('ACCOUNTNO');
            $table->string('ACCOUNTNAME2');
            $table->string('ACCOUNTNO2');
            $table->string('AREAID');
            $table->string('LEGALDEPTNAME');
            $table->string('BANKBRANCH');
            $table->string('BANKNAME');
            $table->string('ISOUTSOURCECALLINGCENTER');
            $table->string('ISOUTSOURCEPRINTWL');
            $table->string('ISUPDATECGID');
            $table->string('USRUPD');
            $table->string('DTMUPD');
            $table->string('SANDIBI2');
            $table->string('PRODUCTIVITYCOLLECTION');
            $table->string('PRODUCTIVITYVALUE');
            $table->string('NUMOFEMPLOYEE');
            $table->string('BANKNAME2');
            $table->string('BANKBRANCH2');
            $table->string('KPPID');
            $table->timestamps();
        });
  }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pretests');
    }
}
