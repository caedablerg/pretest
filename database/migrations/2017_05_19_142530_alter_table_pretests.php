<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePretests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pretests', function (Blueprint $table) {
            $table->string('BBRANCHID')->change();
            $table->string('COMPANYID')->change();
            $table->string('BRANCHAREAPHONE1')->change();
            $table->string('BRANCHPHONE1')->change();
            $table->string('BRANCHAREAPHONE2')->change();
            $table->string('BRANCHAREAFAX')->change();
            $table->string('BRANCHFAX')->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pretests', function (Blueprint $table) {
            //
        });
    }
}
