<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cabangs extends Model
{
    protected $fillable = ['zip','month','lodging','meal'];
}
