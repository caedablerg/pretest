<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cabangs;
use App\Pretest;

class PretestController extends Controller
{

  function convertNumberAbbreviator($value)
    {
        $abbreviations = array(12 => 'T', 9 => 'B', 6 => 'M', 3 => 'K', 0 => '');

        foreach($abbreviations as $exponent => $abbreviation)
        {

            if($value >= pow(10, $exponent))
            {

                return round(($value / pow(10, $exponent)),1).$abbreviation;

            }

        }
      }

    public function showForm()
    {
      return view('upload');
    }  /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //get file
        $upload = $request->file('upload-file');
        $filePath = $upload->getRealPath();

        $file = fopen($filePath,'r');
        $fileContent = file_get_contents($filePath);

        $txt_file    = fopen($filePath,'r');
        $rows        = explode("\n", $fileContent);

          print_r("Loading........");
        foreach($rows as $row => $data)
        {
            //get row data

            $row_data = explode("|", $data);


            $info[$row]['BBRANCHID']           = $row_data[0];
            $info[$row]['CGID']          = $row_data[1];
            $info[$row]['BRANCHFULLNAME']  = $row_data[2];
            $info[$row]['BRANCHINITIALNAME']       = $row_data[3];
            $info[$row]['COMPANYID']       = $row_data[4];
            $info[$row]['BRANCHADDRESS']       = $row_data[5];
            $info[$row]['BRANCHRT']       = $row_data[6];
            $info[$row]['BRANCHRW']       = $row_data[7];
            $info[$row]['BRANCHKELURAHAN']       = $row_data[8];
            $info[$row]['BRANCHKECAMATAN']       = $row_data[9];
            $info[$row]['BRANCHCITY']       = $row_data[10];
            $info[$row]['BRANCHZIPCODE']       = $row_data[11];
            $info[$row]['BRANCHAREAPHONE1']       = $row_data[12];
            $info[$row]['BRANCHPHONE1']       = $row_data[13];
            $info[$row]['BRANCHAREAPHONE2']       = $row_data[14];
            $info[$row]['BRANCHPHONE2']       = $row_data[15];
            $info[$row]['BRANCHAREAFAX']       = $row_data[16];
            $info[$row]['BRANCHFAX']       = $row_data[17];
            $info[$row]['CONTACTPERSONNAME'] =$row_data[18];
            $info[$row]['CONTACTPERSONJOBTITLE']       = $row_data[19];
            $info[$row]['CONTACTPERSONEMAIL']       = $row_data[20];
            $info[$row]['CONTACTPERSONHP']       = $row_data[21];
            $info[$row]['BRANCHMANAGERNAME']       = $row_data[22];
            $info[$row]['ADHNAME']       = $row_data[23];
            $info[$row]['ARCONTROLNAME']       = $row_data[24];
            $info[$row]['ASSETDOCCUSTODIANNAME']       = $row_data[25];
            $info[$row]['BRANCHSTATUS']       = $row_data[26];
            $info[$row]['ISCASHIERCLOSED']       = $row_data[27];
            $info[$row]['ISHEADOFFICE']       = $row_data[28];
            $info[$row]['ACCOUNTNAME']       = $row_data[29];
            $info[$row]['ACCOUNTNO']       = $row_data[30];
            $info[$row]['ACCOUNTNAME2']       = $row_data[31];
            $info[$row]['ACCOUNTNO2']       = $row_data[33];
            $info[$row]['AREAID']       = $row_data[34];
            $info[$row]['LEGALDEPTNAME']       = $row_data[35];
            $info[$row]['BANKBRANCH']       = $row_data[36];
            $info[$row]['BANKNAME']       = $row_data[37];
            $info[$row]['ISOUTSOURCECALLINGCENTER']       = $row_data[38];
            $info[$row]['ISOUTSOURCEPRINTWL']       = $row_data[39];
            $info[$row]['ISUPDATECGID']       = $row_data[40];
            $info[$row]['USRUPD']       = $row_data[41];
            $info[$row]['DTMUPD']       = $row_data[42];
            $info[$row]['SANDIBI2']       = $row_data[43];
            $info[$row]['PRODUCTIVITYCOLLECTION']       = $row_data[44];
            $info[$row]['PRODUCTIVITYVALUE']       = $row_data[45];
            $info[$row]['NUMOFEMPLOYEE']       = $row_data[46];
            $info[$row]['BANKNAME2']       = $row_data[47];
            $info[$row]['BANKBRANCH2']       = $row_data[48];
            $info[$row]['KPPID']       = $row_data[49];

          //Save data
            $pretests = Pretest::create([
              'BBRANCHID' => $info[$row]['BBRANCHID'],
              'CGID' => $info[$row]['CGID'],
              'BRANCHFULLNAME' => $info[$row]['BRANCHFULLNAME'],
              'BRANCHINITIALNAME' => $info[$row]['BRANCHINITIALNAME'],
              'COMPANYID' => $info[$row]['COMPANYID'],
              'BRANCHADDRESS' => $info[$row]['BRANCHADDRESS'],
              'BRANCHRT' => $info[$row]['BRANCHRT'],
              'BRANCHRW' => $info[$row]['BRANCHRW'],
              'BRANCHKELURAHAN' => $info[$row]['BRANCHKELURAHAN'],
              'BRANCHKECAMATAN' => $info[$row]['BRANCHKECAMATAN'],
              'BRANCHCITY' => $info[$row]['BRANCHCITY'],
              'BRANCHZIPCODE' => $info[$row]['BRANCHZIPCODE'],
              'BRANCHAREAPHONE1' => $info[$row]['BRANCHAREAPHONE1'],
              'BRANCHPHONE1' => $info[$row]['BRANCHPHONE1'],
              'BRANCHAREAPHONE2' => $info[$row]['BRANCHAREAPHONE2'],
              'BRANCHPHONE2' => $info[$row]['BRANCHPHONE2'],
              'BRANCHAREAFAX' => $info[$row]['BRANCHAREAFAX'],
              'BRANCHFAX' => $info[$row]['BRANCHFAX'],
              'CONTACTPERSONNAME' => $info[$row]['CONTACTPERSONNAME'],
              'CONTACTPERSONJOBTITLE' => $info[$row]['CONTACTPERSONJOBTITLE'],
              'CONTACTPERSONEMAIL' => $info[$row]['CONTACTPERSONEMAIL'],
              'CONTACTPERSONHP' => $info[$row]['CONTACTPERSONHP'],
              'BRANCHMANAGERNAME' => $info[$row]['BRANCHMANAGERNAME'],
              'ADHNAME' => $info[$row]['ADHNAME'],
              'ARCONTROLNAME' => $info[$row]['ARCONTROLNAME'],
              'ASSETDOCCUSTODIANNAME' => $info[$row]['ASSETDOCCUSTODIANNAME'],
              'BRANCHSTATUS' => $info[$row]['BRANCHSTATUS'],
              'ISCASHIERCLOSED' => $info[$row]['ISCASHIERCLOSED'],
              'ISHEADOFFICE' => $info[$row]['ISHEADOFFICE'],
              'ACCOUNTNAME' => $info[$row]['ACCOUNTNAME'],
              'ACCOUNTNO' => $info[$row]['ACCOUNTNO'],
              'ACCOUNTNAME2' => $info[$row]['ACCOUNTNAME2'],
              'ACCOUNTNO2' => $info[$row]['ACCOUNTNO2'],
              'AREAID' => $info[$row]['AREAID'],
              'LEGALDEPTNAME' => $info[$row]['LEGALDEPTNAME'],
              'BANKBRANCH' => $info[$row]['BANKBRANCH'],
              'BANKNAME' => $info[$row]['BANKNAME'],
              'ISOUTSOURCECALLINGCENTER' => $info[$row]['ISOUTSOURCECALLINGCENTER'],
              'ISOUTSOURCEPRINTWL' => $info[$row]['ISOUTSOURCEPRINTWL'],
              'ISUPDATECGID' => $info[$row]['ISUPDATECGID'],
              'USRUPD' => $info[$row]['USRUPD'],
              'DTMUPD' => $info[$row]['DTMUPD'],
              'SANDIBI2' => $info[$row]['SANDIBI2'],
              'PRODUCTIVITYCOLLECTION' => $info[$row]['PRODUCTIVITYCOLLECTION'],
              'PRODUCTIVITYVALUE' => $info[$row]['PRODUCTIVITYVALUE'],
              'NUMOFEMPLOYEE' => $info[$row]['NUMOFEMPLOYEE'],
              'BANKNAME2' => $info[$row]['BANKNAME2'],
              'BANKBRANCH2' => $info[$row]['BANKBRANCH2'],
              'KPPID' => $info[$row]['KPPID'],

            ]);
        }
        return redirect('/');
    }


    public function search($keyword)
    {
        $data = Pretest::where('BRANCHFULLNAME','like','%'.$keyword.'%')->orderBy('id')->paginate(3);
        return response()->json($data);

    }
}
